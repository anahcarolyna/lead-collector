package br.com.lead.collector.models;

import br.com.lead.collector.enums.TipoLeadEnum;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.List;

@Entity
//@Table(name = "table")
public class Lead {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @JsonIgnoreProperties(value = {"data"}, allowGetters = true)
    private int id;

    //se existir a tabela
    //@Column(name = "nome_completo")

    @Size(min = 5, max = 100, message = "O nome deve ter entre 5 a  100 caracteres")
    private String nome;

    @Email(message = "O formato do email é inválido")
    @NotNull(message = "Email não pode ser nulo")
    private String email;

    private LocalDate data;

    @Enumerated(EnumType.STRING)
    private TipoLeadEnum tipoLead;

    @ManyToMany(cascade = CascadeType.ALL)
   // @JoinTable(name = "produto_lead", joinColumns = @JoinColumn(name =  "lead_id"), inverseJoinColumns = @JoinColumn(name = "product_id")))
    private List<Produto> produtos;

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDataDeCadastro() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    public TipoLeadEnum getTipoLead() {
        return tipoLead;
    }

    public void setTipoLead(TipoLeadEnum tipoLead) {
        this.tipoLead = tipoLead;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getData() {
        return data;
    }

    public List<Produto> getProdutos() {
        return produtos;
    }

    public void setProdutos(List<Produto> produtos) {
        this.produtos = produtos;
    }
}

