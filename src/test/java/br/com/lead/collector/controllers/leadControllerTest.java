package br.com.lead.collector.controllers;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.service.LeadService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@WebMvcTest(LeadController.class)
public class leadControllerTest {

    @MockBean
    private LeadService leadService;

    @Autowired
    private MockMvc mockMvc;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setNome("Carol");
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("carol@gmail.com");

        produto = new Produto();
        produto.setDescricao("Chá para emagrecimento");
        produto.setId(1);
        produto.setNome("Cha Desincha");
        produto.setPreco(30.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarRegistrarLeadValido() throws Exception {
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).then(leadObjeto -> {
            lead.setId(1);
            lead.setData(LocalDate.now());
            return lead;
        });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.post("/leads")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data", CoreMatchers.equalTo((LocalDate.now().toString()))));
    }

    @Test
    public void testarExibirTodosOsLeads() throws Exception {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadService.buscarTodos()).thenReturn(leads);

               mockMvc.perform(MockMvcRequestBuilders.get("/leads")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarExibirTodosOsLeadsPorTipoDeLead() throws Exception {
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadService.buscarTodosPorTipoLead(Mockito.any(TipoLeadEnum.class))).thenReturn(leads);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads?tipoDeLead=QUENTE")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.length()").value(1));
    }

    @Test
    public void testarBuscarPorIdComSucesso() throws Exception {
        lead.setId(1);
        Mockito.when(leadService.buscarPorID(Mockito.anyInt())).thenReturn(lead);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/1")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)));
    }

    @Test
    public void testarBuscarPorIdNaoEncontrado() throws Exception {
        Lead leadObjeto = new Lead();
        Mockito.when(leadService.buscarPorID(Mockito.anyInt())).thenThrow(RuntimeException.class);

        mockMvc.perform(MockMvcRequestBuilders.get("/leads/50")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarLeadNaoEncontrado() throws Exception {
        lead.setId(1);
        lead.setData(LocalDate.now());
        Mockito.when(leadService.salvarLead(Mockito.any(Lead.class))).thenThrow(RuntimeException.class);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead))
                .andExpect(MockMvcResultMatchers.status().isBadRequest());
    }

    @Test
    public void testarAtualizarLeadComSucesso() throws Exception {
        //lead.setId(1);
        lead.setData(LocalDate.now());
        Mockito.when(leadService.atualizarLead(Mockito.anyInt(), Mockito.any(Lead.class))).thenReturn(lead);

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeLead = mapper.writeValueAsString(lead);

        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.put("/leads/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeLead)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        String content = mvcResult.getResponse().getContentAsString();
    }

}
