package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.LeadRepository;
import br.com.lead.collector.service.LeadService;
import br.com.lead.collector.service.ProdutoService;
import org.aspectj.lang.annotation.Before;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Answers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class LeadServiceTest {

    @MockBean
    private LeadRepository leadRepository;

    @MockBean
    private ProdutoService produtoService;

    @Autowired
    private LeadService leadService;

    Lead lead;
    Produto produto;
    List<Produto> produtos;

    @BeforeEach
    public void setUp(){
        lead = new Lead();
        lead.setNome("Vinicius");
        lead.setData(LocalDate.now());
        lead.setTipoLead(TipoLeadEnum.QUENTE);
        lead.setEmail("carol@gmail.com");

        produto = new Produto();
        produto.setDescricao("Chá para emagrecimento");
        produto.setId(1);
        produto.setNome("Cha Desincha");
        produto.setPreco(30.00);

        produtos = new ArrayList<>();
        produtos.add(produto);

        lead.setProdutos(produtos);
    }

    @Test
    public void testarBuscarPorTodosOsLeads(){
        Iterable<Lead> leads = Arrays.asList(lead);
        Mockito.when(leadRepository.findAll()).thenReturn(leads);

        Iterable<Lead> leadIterable = leadService.buscarTodos();

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarSalvarLead(){
        Mockito.when(produtoService.buscarTodosPorID(Mockito.anyList())).thenReturn(produtos);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);

        /* Quando quer ver o retorno do ID preenchido - Mocito com lambda
        Mockito.when(leadRepository.save(leadTeste)).then(leadLamb -> {
            leadTeste.setId(1);
            return leadTeste;
        });
        */
        Mockito.when(leadRepository.save(leadTeste)).thenReturn(leadTeste);

        Lead leadObjeto = leadService.salvarLead(leadTeste);

        Assertions.assertEquals(LocalDate.now(), leadObjeto.getData());
        Assertions.assertEquals(produto, leadObjeto.getProdutos().get(0));
    }

    @Test
    public void testarBuscarPorTipoLead(){
        Iterable<Lead> leads= Arrays.asList(lead);
        Mockito.when(leadRepository.findAllByTipoLead(Mockito.any(TipoLeadEnum.class))).thenReturn(leads);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);

        Iterable<Lead> leadIterable = leadService.buscarTodosPorTipoLead(leadTeste.getTipoLead());

        Assertions.assertEquals(leads, leadIterable);
    }

    @Test
    public void testarBuscarPorID(){
        Optional<Lead> optionalLead =  Optional.of(lead);
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(optionalLead);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);

        Lead leadObjeto = leadService.buscarPorID(leadTeste.getId());

        Assertions.assertEquals(leadObjeto, optionalLead.get());

    }

    @Test
    public void testarBuscarPorIDNaoEncontrado(){
        Optional<Lead> optionalLead =  Optional.empty();
        Mockito.when(leadRepository.findById(Mockito.anyInt())).thenReturn(optionalLead);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.buscarPorID(leadTeste.getId());});
    }

    @Test
    public void testarAtualizarLeadNaoEncontrado(){
        boolean existeLead = false;
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(existeLead);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);
        leadTeste.setId(2);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.atualizarLead(leadTeste.getId(), leadTeste);});
    }

    @Test
    public void testarAtualizarLeadComSucesso(){
        boolean existeLead = true;
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(existeLead);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);
        leadTeste.setId(2);

        Mockito.when(leadRepository.save(leadTeste)).thenReturn(leadTeste);

        Lead leadObjeto = leadService.atualizarLead(1, leadTeste);

        Assertions.assertSame(leadObjeto, leadTeste);
    }

    @Test
    public void testarDeletarLeadNaoEncontrado(){
        boolean existeLead = false;
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(existeLead);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);
        leadTeste.setId(2);

        Assertions.assertThrows(RuntimeException.class, () -> {leadService.deletarLead(leadTeste.getId());});
    }

    @Test
    public void testarDeletarLeadComSucesso(){
        boolean existeLead = true;
        Mockito.when(leadRepository.existsById(Mockito.anyInt())).thenReturn(existeLead);

        Lead leadTeste = new Lead();
        leadTeste.setNome("Carlos");
        leadTeste.setTipoLead(TipoLeadEnum.FRIO);
        leadTeste.setEmail("carol@gmail.com");
        leadTeste.setProdutos(produtos);
        leadTeste.setId(2);

        leadService.deletarLead(leadTeste.getId());
        Mockito.verify(leadRepository, Mockito.times(1)).deleteById(leadTeste.getId());

    }

}
