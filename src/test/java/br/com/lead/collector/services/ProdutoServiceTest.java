package br.com.lead.collector.services;

import br.com.lead.collector.enums.TipoLeadEnum;
import br.com.lead.collector.models.Lead;
import br.com.lead.collector.models.Produto;
import br.com.lead.collector.repositories.ProdutoRepository;
import br.com.lead.collector.service.ProdutoService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class ProdutoServiceTest {

    @MockBean
    private ProdutoRepository produtoRepository;

    @Autowired
    private ProdutoService produtoService;

    Produto produto;

    @BeforeEach
    public void setUp(){
        produto = new Produto();
        produto.setDescricao("Chá para emagrecimento");
        produto.setId(1);
        produto.setNome("Cha Desincha");
        produto.setPreco(30.00);
    }

    @Test
    public void testarBuscarTodosProdutos(){
        Iterable<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAll()).thenReturn(produtos);

        Iterable<Produto> produtoObjeto = produtoService.buscarTodos();

        Assertions.assertEquals(produtos, produtoObjeto);
    }

    @Test
    public void testarSalvarProduto(){
        Produto produtoTeste = new Produto();
        produtoTeste.setDescricao("Chá para engordar");
        produtoTeste.setId(1);
        produtoTeste.setNome("Cha Incha");
        produtoTeste.setPreco(30.00);

        Mockito.when(produtoRepository.save(produtoTeste)).thenReturn(produtoTeste);

        Produto produtoObjeto = produtoService.salvarProduto(produtoTeste);

        Assertions.assertEquals(produtoTeste, produtoObjeto);
    }

    @Test
    public void testarBuscarTodosPorNome(){
        Iterable<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAllByNomeContaining(Mockito.anyString())).thenReturn(produtos);

        Produto produtoTeste = new Produto();
        produtoTeste.setDescricao("Chá para engordar");
        produtoTeste.setId(1);
        produtoTeste.setNome("Cha Incha");
        produtoTeste.setPreco(30.00);

        Iterable<Produto> produtoIterable = produtoService.buscarTodosPorNome(produtoTeste.getNome());

        Assertions.assertEquals(produtos, produtoIterable);
    }

    @Test
    public void testarBuscarTodosPorId(){
        Iterable<Produto> produtos = Arrays.asList(produto);
        Mockito.when(produtoRepository.findAllById(Mockito.anyList())).thenReturn(produtos);

        Iterable<Produto> produtoIterable = produtoService.buscarTodosPorID(Mockito.anyList());
        Assertions.assertEquals(produtos, produtoIterable);
    }

    @Test
    public void testarBuscarPorIdNaoEncontrado(){
        Optional<Produto> optionalProduto =  Optional.empty();
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(optionalProduto);

        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.buscarPorID(Mockito.anyInt());});
    }

    @Test
    public void testarBuscarPorId(){
        Optional<Produto> optionalProduto =  Optional.of(produto);
        Mockito.when(produtoRepository.findById(Mockito.anyInt())).thenReturn(optionalProduto);

        Produto ProdutoObjeto = produtoService.buscarPorID(Mockito.anyInt());

        Assertions.assertEquals(ProdutoObjeto, optionalProduto.get());
    }

    @Test
    public void testarAtualizarProdutoNaoEncontrado(){
        boolean existeProduto = false;
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(existeProduto);

        Produto produtoTeste = new Produto();
        produtoTeste.setDescricao("Chá para engordar");
        produtoTeste.setId(1);
        produtoTeste.setNome("Cha Incha");
        produtoTeste.setPreco(30.00);

        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.atualizarProduto(produtoTeste.getId(), produtoTeste);});
    }

    @Test
    public void testarAtualizarProdutoComSucesso(){
        boolean existeProduto= true;
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(existeProduto);

        Produto produtoTeste = new Produto();
        produtoTeste.setDescricao("Chá para engordar");
        produtoTeste.setId(1);
        produtoTeste.setNome("Cha Incha");
        produtoTeste.setPreco(30.00);

        Mockito.when(produtoRepository.save(produtoTeste)).thenReturn(produtoTeste);

        Produto produtoObjeto = produtoService.atualizarProduto( produtoTeste.getId(), produtoTeste);

        Assertions.assertSame(produtoTeste, produtoObjeto);
    }

    @Test
    public void testarDeletarProdutoNaoEncontrado(){
        boolean existeProduto = false;
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(existeProduto);

        Assertions.assertThrows(RuntimeException.class, () -> {produtoService.deletarProduto(Mockito.anyInt());});
    }

    @Test
    public void testarDeletarProdutoComSucesso(){
        boolean existeProduto= true;
        Mockito.when(produtoRepository.existsById(Mockito.anyInt())).thenReturn(existeProduto);

        produtoService.deletarProduto(Mockito.anyInt());
        Mockito.verify(produtoRepository, Mockito.times(1)).deleteById(Mockito.anyInt());

    }
}
